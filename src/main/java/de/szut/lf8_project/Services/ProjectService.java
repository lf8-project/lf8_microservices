package de.szut.lf8_project.Services;

import de.szut.lf8_project.Model.Project;
import de.szut.lf8_project.Repository.ProjectRepository;
import de.szut.lf8_project.exceptionhandling.ResourceNotFoundException;
import org.springframework.stereotype.Service;
//import de.szut.lf8_project.Model.Project;
//import de.szut.lf8_project.Repository.ProjectRepository;


import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProjectService {

    final ProjectRepository repository;

    public ProjectService(ProjectRepository repository) {
        this.repository = repository;
    }

    public Project create(Project newProject) {
        return repository.save(newProject);
    }

    public Project update(Project Project, long id){
        Project updatedProject = readById(id);
        updatedProject.setHeadEmployeeID(Project.getHeadEmployeeID());
        updatedProject.setCustomerEmployee(Project.getCustomerEmployee());
        updatedProject.setComment(Project.getComment());
        updatedProject.setStartDate(Project.getStartDate());
        updatedProject.setEstimatedEnd(Project.getEstimatedEnd());
        updatedProject.setActualEnd(Project.getActualEnd());
        updatedProject.setEmployeeList(Project.getEmployeeList());
        repository.save(updatedProject);
        return updatedProject;
    }

    public void delete(Long id) {
        Optional<Project> s = repository.findById(id);

        if (s.isEmpty())
            throw new ResourceNotFoundException("Project not found = " + id );
        else
            repository.deleteById(id);
    }

    public List<Project> readAll(){
        return repository.findAllByOrderByIdDesc();
    }

    public Project readById(Long id){
        Optional<Project> s = repository.findById(id);

        if (s.isEmpty())
            throw new ResourceNotFoundException("Project not found = " + id );

        return s.get();
    }

    public Project addEmployeeToProject(Project project, long id) {
        Project updatedProject = readById(project.getId());
        Set<Long> employeeList = project.getEmployeeList();
        if (employeeList.contains(id) )
            throw new ResourceNotFoundException("Project already contains Employee with ID = " + id );
        else{
            employeeList.add(id);
            updatedProject.setEmployeeList(employeeList);
            repository.save(updatedProject);
            return updatedProject;
        }

    }

    public Project deleteEmployeeFromProject(Project project, long id) {
        Project updatedProject = readById(project.getId());
        Set<Long> employeeList = project.getEmployeeList();
        if (employeeList.contains(id) ){
            employeeList.remove(id);
            updatedProject.setEmployeeList(employeeList);
            repository.save(updatedProject);
            return updatedProject;
        }
        else{
            throw new ResourceNotFoundException("Project has no Employee with ID = " + id );
        }

    }

    public List<Project> findEmployeeProjects(long id) {
        List<Project> allProjects = readAll();
        List<Project> employeeProjects = readAll();
        for (Project project : allProjects) {
            if (project.getEmployeeList().contains(id)) {
                employeeProjects.add(project);
            }
        }
        return employeeProjects;
    }

    public boolean employeeAvailable(Project newProject, long id) {
        List<Project> employeeProjects = findEmployeeProjects(id);
        for (Project project : employeeProjects) {
            if ((newProject.getStartDate().isAfter(project.getStartDate()) && newProject.getStartDate().isBefore(project.getEstimatedEnd())) || (newProject.getEstimatedEnd().isAfter(project.getStartDate()) && newProject.getEstimatedEnd().isBefore(project.getEstimatedEnd()))) {
                return false;
            }
        }
        return true;
    }
}
