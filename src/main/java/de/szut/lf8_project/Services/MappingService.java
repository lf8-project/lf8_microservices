package de.szut.lf8_project.Services;
import de.szut.lf8_project.DTO.AddProjectDto;
import de.szut.lf8_project.DTO.GetProjectDto;
import de.szut.lf8_project.Model.Project;
import org.springframework.stereotype.Service;

@Service
public class MappingService {

    public Project mapAddProjectDtoToProject(AddProjectDto dto){
        Project newproject = new Project();
        newproject.setDescription(dto.getDescription());
        newproject.setComment(dto.getComment());
        newproject.setCustomerID(dto.getCustomerID());
        newproject.setEmployeeList(dto.getEmployeeList());
        newproject.setCustomerEmployee(dto.getCustomerEmployee());
        newproject.setEstimatedEnd(dto.getEstimatedEnd());
        newproject.setStartDate(dto.getStartDate());
        newproject.setHeadEmployeeID(dto.getHeadEmployeeID());
        return newproject;
    }

    public GetProjectDto mapProjectToGetProjectDto(Project project){
        GetProjectDto newGetProjectDto = new GetProjectDto();
        newGetProjectDto.setDescription(project.getDescription());
        newGetProjectDto.setComment(project.getComment());
        newGetProjectDto.setActualEnd(project.getActualEnd());
        newGetProjectDto.setCustomerEmployee(project.getCustomerEmployee());
        newGetProjectDto.setCustomerID(project.getCustomerID());
        newGetProjectDto.setEstimatedEnd(project.getEstimatedEnd());
        newGetProjectDto.setStartDate(project.getStartDate());
        newGetProjectDto.setHeadEmployeeID(project.getHeadEmployeeID());
        return newGetProjectDto;
    }
}