package de.szut.lf8_project.Model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Description is mandatory")
    private String description;

    @NotNull
    private Long headEmployeeID;

    @NotNull
    private Long customerID;
    @NotNull
    private String customerEmployee;

    @NotBlank(message = "Comment is mandatory")
    private String comment;

    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @Column(name = "estimated_end_date", nullable = false)
    private LocalDate estimatedEnd;

    private LocalDate actualEnd;

    @ElementCollection
    private Set<Long> EmployeeList;
}

// projectTimePeriod = range(startDate, estimatedEnd)
// if(startDate in projectTimePeriod || estimatedEnd in projectTimePeriod)  -> Error