package de.szut.lf8_project.Controller;


import de.szut.lf8_project.DTO.AddProjectDto;
import de.szut.lf8_project.DTO.GetProjectDto;
import de.szut.lf8_project.Model.Project;
import de.szut.lf8_project.Repository.ProjectRepository;
import de.szut.lf8_project.Services.MappingService;
import de.szut.lf8_project.Services.ProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService service;
    private final MappingService mappingService;

    public ProjectController(ProjectService projectService, MappingService mappingService) {
        this.service = projectService;
        this.mappingService = mappingService;
    }

    @PostMapping
    public ResponseEntity<GetProjectDto> createProject(@Valid @RequestBody final AddProjectDto dto) {
        Project newProject = this.mappingService.mapAddProjectDtoToProject(dto);
        newProject = this.service.create(newProject);
        final GetProjectDto request = this.mappingService.mapProjectToGetProjectDto(newProject);
        return new ResponseEntity<>(request, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetProjectDto> getProjectById(@PathVariable final Long id) {
        final var entity = this.service.readById(id);
        final GetProjectDto dto = this.mappingService.mapProjectToGetProjectDto(entity);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteProject(@PathVariable final Long id) {
        this.service.delete(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<GetProjectDto> updateProject(@Valid @RequestBody final AddProjectDto dto, @PathVariable final long id) {
        Project newProject =
                this.mappingService.mapAddProjectDtoToProject(dto);
        newProject = this.service.update(newProject, id);
        final GetProjectDto request = this.mappingService.mapProjectToGetProjectDto(newProject);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }


    @GetMapping
    public ResponseEntity<List<GetProjectDto>> findAllProjects() {
        List<Project> all = this.service.readAll();
        List<GetProjectDto> dtoList = new LinkedList<>();
        for (Project project : all) {
            dtoList.add(this.mappingService.mapProjectToGetProjectDto(project));
        }
        return new ResponseEntity<>(dtoList, HttpStatus.OK);
    }

    @PostMapping("/{pid}/{eid}")
    public ResponseEntity<GetProjectDto> addEmployeeToProject(@PathVariable final Long pid, @PathVariable final Long eid) {
        Project project = this.service.readById(pid);
        GetProjectDto request = this.mappingService.mapProjectToGetProjectDto(project);

        if (this.service.employeeAvailable(project, eid)) {
            project = this.service.addEmployeeToProject(project, eid);
            request = this.mappingService.mapProjectToGetProjectDto(project);
            return new ResponseEntity<>(request, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(request, HttpStatus.PRECONDITION_FAILED);
        }
    }

    @DeleteMapping("{pid}/{eid}")
    public ResponseEntity<GetProjectDto> deleteEmployeeFromProject(@PathVariable final Long pid, @PathVariable final Long eid) {
        Project project = this.service.readById(pid);
        project = this.service.deleteEmployeeFromProject(project, eid);
        final GetProjectDto request = this.mappingService.mapProjectToGetProjectDto(project);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @GetMapping("/{id}/employees")
    public ResponseEntity<Set<Long>> getEmployeesByProjectId(@PathVariable final Long id) {
        final var entity = this.service.readById(id);
        final Set<Long> list = entity.getEmployeeList();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}

