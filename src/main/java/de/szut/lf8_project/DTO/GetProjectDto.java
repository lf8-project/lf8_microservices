package de.szut.lf8_project.DTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Data
public class GetProjectDto {
    private String description;
    private Long headEmployeeID;
    private Long customerID;
    private String customerEmployee;
    private String comment;
    private LocalDate startDate;
    private LocalDate estimatedEnd;
    private LocalDate actualEnd;
}
