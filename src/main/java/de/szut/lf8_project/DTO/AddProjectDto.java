package de.szut.lf8_project.DTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Data
public class AddProjectDto {
    @NotBlank(message = "Description is mandatory")
    @Size(max = 50, message = "Description must not exceed 50 characters")
    private String description;

    @NotNull(message = "Head Employee ID is mandatory")
    private Long headEmployeeID;

    @NotNull(message = "Customer ID is mandatory")
    private Long customerID;

    @NotEmpty(message = "Name of the Customer Employee is mandatory")
    private String customerEmployee;

    private String comment;

    @NotEmpty(message = "Start Date is mandatory")
    private LocalDate startDate;

    @NotEmpty(message = "Estimated End is mandatory")
    private LocalDate estimatedEnd;

    private Set<Long> EmployeeList;
}
